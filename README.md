# https://nerd.vision Express demo application

## Intro

This application is an example of how to use the nerd.vision Node agent with a simple express todo app.

## Requirements

* Node 10+

## Installation

1. Clone this repo
1. Run `npm i`
1. Copy the `.env.example` file to `.env`
1. Get your API key from [account](https://account.nerd.vision/account/apikeys) and put it in `.env` as the value for `NV_API_KEY`
1. Run the application `npm run start`
1. You can now visit and use the application at http://localhost:3000
