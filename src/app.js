const express = require('express');
const sassMiddleware = require('node-sass-middleware');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const routes = require('./routes');
const models = require('./models');
const {join} = require('path');
const app = express();

module.exports = async (port = 3000) => {
    await models.sequelize.sync();

    app.use(sassMiddleware({
        /* Options */
        src : __dirname,
        dest : join(__dirname, 'public'),
        debug : true,
        outputStyle : 'compressed',
        prefix : '/public'  // Where prefix is at <link rel="stylesheets" href="prefix/style.css"/>
    }));
    app.use('/public', express.static(join(__dirname, 'public')));

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended : false}));
    app.use(cookieParser());
    app.use(methodOverride('_method'));

    app.set('views', './src/views');
    app.set('view engine', 'pug');

    app.use('/', routes);

    app.get('*', (req, res) => {
        res.render('404', {title : '404 - page not found'});
    });

    app.listen(port, () => console.log(`Listening on http://0.0.0.0:${port}`));
};
