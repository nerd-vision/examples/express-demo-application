const {Todo} = require('../models');

module.exports = class HomeController {
    static async index(req) {
        const todos = await Todo.findAll({where : {complete : false}});
        const completed = await Todo.findAll({where : {complete : true}});

        return {
            title : 'Todo list',
            todos,
            completed,
        };
    }
};
