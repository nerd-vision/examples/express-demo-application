const {Todo} = require('../models');

module.exports = class HomeController {
    static async create(req) {
        if (!req.body.title.trim()) {
            return;
        }

        await Todo.create({
            title : req.body.title,
            complete : false,
        });
    }

    static async complete(req) {
        const todo = await Todo.findByPk(req.params.todo_id);

        if (!todo) {
            throw new Error('Todo not found');
        }

        todo.complete = req.body.complete;

        await todo.save();
    }

    static async delete(req) {
        const params = {...req.params};

        await Todo.destroy({
            where : {id : params.id}
        });
    }
};
