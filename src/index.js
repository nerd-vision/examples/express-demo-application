const {nerdvision} = require('@nerdvision/agent');
const app = require('./app');

require('dotenv').config();

nerdvision.init(process.env.NV_API_KEY)
    .then(() => app(process.env.PORT))
    .catch(e => console.error(e));
