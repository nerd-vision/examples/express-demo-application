const home = require('./controllers/home.controller');
const todo = require('./controllers/todo.controller');

const express = require('express');
const router = express.Router();

const multer = require('multer');
const upload = multer();

router.get('/', async function (req, res) {
    res.render('home/index', await home.index(req));
});

router.post('/todos', upload.none(), async (req, res) => {
    await todo.create(req);
    res.redirect('back');
});

router.put('/todos/:todo_id/complete', upload.none(), async (req, res) => {
    await todo.complete(req);

    res.status(204).send();
});

router.delete('/todos/:todo_id', async (req, res) => {
    try {
        await todo.delete(req);
    } catch (e) {

    }

    res.redirect('back');
});

module.exports = router;
